﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Notebook
{

    class Note
    {
        private string lastName;
        public string LastName { get { return lastName; } set { if (value != "") lastName = value; } }

        private string name;
        public string Name { get { return name; } set { if (value != "") name = value; } }
        public string MiddleName { get; set; }

        private string phone;
        public string Phone { get { return phone; } set { if (value != "") phone = value; } }

        private string country;
        public string Country { get { return country; } set { if (value != "") country = value; } }

        public DateTime? BirthDate { get; set; }

        public string Org { get; set; }

        public string Job { get; set; }

        public string Etc { get; set; }
        public Note(string ln, string n, string mn, string pn, string c, DateTime? bd, string o, string j, string e)
        {
            LastName = ln;
            Name = n;
            MiddleName = mn;
            Phone = pn;
            Country = c;
            BirthDate = bd;
            Org = o;
            Job = j;
            Etc = e;
        }

        public override string ToString()
        {
            StringBuilder t = new StringBuilder();
            t.Append("Фамилия: ");
            t.Append(LastName);
            t.Append("\nИмя: ");
            t.Append(Name);
            if (MiddleName != "")
            {
                t.Append("\nОтчество: ");
                t.Append(MiddleName);
            }
            t.Append("\nТелефон: ");
            t.Append(Phone);
            t.Append("\nСтрана: ");
            t.Append(Country);
            if (BirthDate != null)
            {
                t.Append("\nДата рождения: ");
                t.Append(BirthDate.Value.ToShortDateString());
            }
            if (Org != "")
            {
                t.Append("\nОрганизация: ");
                t.Append(Org);
            }
            if (Job != "")
            {
                t.Append("\nДолжность: ");
                t.Append(Job);
            }
            if (Etc != "")
            {
                t.Append("\nЗаметки: ");
                t.Append(Etc);
            }

            return t.ToString();
        }
        public string ToShortString()
        {
            StringBuilder t = new StringBuilder();
            t.Append("Фамилия: ");
            t.Append(LastName);
            t.Append(" \tИмя: ");
            t.Append(Name);
            t.Append(" \tТелефон: ");
            t.Append(Phone);
            return t.ToString();
        }


    }
}
