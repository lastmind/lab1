﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;

namespace Notebook
{
    class Program
    {
        const int menuNum = 5;
        static List<Note> ALL = new List<Note>();

        static void Main(string[] args)
        {
            MenuChoose deed;
            while (true)
            {
                Text(Texts.main);
                if (!Menu(out deed))
                {
                    Console.Clear();
                    Text(Texts.error);
                    continue;
                }

                switch (deed)
                {
                    case MenuChoose.create:
                        Create();
                        break;
                    case MenuChoose.edit:
                        Edit();
                        break;
                    case MenuChoose.cat:
                        Cat();
                        break;
                    case MenuChoose.delete:
                        Delete();
                        break;
                    case MenuChoose.la:
                        La();
                        break;
                    case MenuChoose.exit:
                        return;
                }
            }
        }
        static bool Menu(out MenuChoose deed)
        {
            int t;
            int.TryParse(Console.ReadLine(), out t);
            deed = (MenuChoose)t;
            return ((t >= 0) && (t <= menuNum));
        }
        static bool ChooseNote(out int a)
        {
            int.TryParse(Console.ReadLine(), out a);
            return ((a >= -1) && (a <= ALL.Count()));
        }
        static bool ChooseEdit(out int a)
        {
            int.TryParse(Console.ReadLine(), out a);
            return ((a >= 0) && (a <= 9));
        }
        static void Create()
        {
            Console.Clear();
            Text(Texts.create);
            Console.Write("Фамилия*: ");
            string lastname;
            while((lastname=Console.ReadLine())=="")
            {
                Text(Texts.error);
            }
            Console.Write("Имя*: ");
            string name;
            while ((name = Console.ReadLine()) == "")
            {
                Text(Texts.error);
            }
            Console.Write("Отчество: ");
            string middlename = Console.ReadLine(); //
            Console.Write("Номер телефона*: ");
            string phone;
            while (((phone = Console.ReadLine()) == "")||
                !((phone.Substring(1).All((char tc) => (tc >= '0') && (tc <= '9'))) && 
                    (((phone[0] >= '0') && (phone[0] <= '9')) || (phone[0] == '+'))))
            {
                Text(Texts.error);
            }
            Console.Write("Страна*: ");
            string country;
            while ((country = Console.ReadLine()) == "")
            {
                Text(Texts.error);
            }
            Console.Write("Дата рождения: ");
            string birthday = Console.ReadLine(); //
            DateTime? birthdaydate;
            DateTime t;
            while (!DateTime.TryParse(birthday, out t) && (birthday != ""))
            {
                Text(Texts.error);
                Console.Write("Дата рождения: ");
                birthday = Console.ReadLine();
            }
            if (birthday != "") birthdaydate = t;
            else birthdaydate = null;

            Console.Write("Организация: ");
            string org = Console.ReadLine(); //
            Console.Write("Должность: ");
            string job = Console.ReadLine(); //
            Console.Write("Заметки: ");
            string etc = Console.ReadLine(); //
            ALL.Add(new Note(lastname, name, middlename, phone, country, birthdaydate, org, job, etc));
            Text(Texts.success);
            Text(Texts.cont);
            Console.Clear();
        }
        static void Edit()  //FINISH
        {
            Console.Clear();
            Text(Texts.edit);
            int deed;
            while (!ChooseNote(out deed))
            {
                Console.Clear();
                Text(Texts.error);
                Text(Texts.edit);
                continue;
            }
            if (deed == -1) { La(); Edit(); }
            else if (deed == 0) { Console.Clear(); return; }
            else
            {
                Console.WriteLine(ALL[deed - 1]);
                Text(Texts.props);
                int edit;
                while (!ChooseEdit(out edit))
                {
                    Console.Clear();
                    Text(Texts.error);
                    Console.WriteLine(ALL[deed - 1]);
                    Text(Texts.props);
                    continue;
                }
                if (edit == 0) { Console.Clear(); return; }
                else
                {
                    Console.WriteLine("Введите новое значение: ");
                    EditNote(edit, Console.ReadLine(), deed-1);
                    Text(Texts.success);
                    Text(Texts.cont);
                    Console.Clear();
                }
            }
        }
        static void EditNote(int n, string s, int obj)
        {
            switch(n)
            {
                case 1:
                    ALL[obj].LastName = s;
                    break;
                case 2:
                    ALL[obj].Name = s;
                    break;
                case 3:
                    ALL[obj].MiddleName = s;
                    break;
                case 4:
                    if ((s.Substring(1).All((char t) => (t >= '0') && (t <= '9'))) && (((s[0] >= '0') && (s[0] <= '9')) || (s[0] == '+')))
                        ALL[obj].Phone = s;
                    break;
                case 5:
                    ALL[obj].Country = s;
                    break;
                case 6:
                    DateTime d;
                    if(DateTime.TryParse(s, out d)) 
                        ALL[obj].BirthDate = d;
                    if (s == "") ALL[obj].BirthDate = null;
                    break;
                case 7:
                    ALL[obj].Org = s;
                    break;
                case 8:
                    ALL[obj].Job = s;
                    break;
                case 9:
                    ALL[obj].Etc = s;
                    break;

            }
        }
        static void Cat()
        {
            Console.Clear();
            Text(Texts.cat);
            int deed;
            while (!ChooseNote(out deed))
            {
                Console.Clear();
                Text(Texts.error);
                Text(Texts.edit);
                continue;
            }
            if (deed == -1) { La(); Cat(); }
            else if (deed == 0) { Console.Clear(); return; }
            else
            {
                Console.WriteLine(ALL[deed - 1]);
                Text(Texts.cont);
                Console.Clear();
            }
        }
        static void La()
        {
            Console.Clear();
            //Text(Texts.la);
            int c = 1;
            foreach (Note note in ALL)
            {
                Console.WriteLine((c++) +". "+ note.ToShortString());
            }
            Text(Texts.cont);
            Console.Clear();
        }
        static void Delete()
        {
            Console.Clear();
            Text(Texts.delete);
            int deed;
            while (!ChooseNote(out deed))
            {
                Console.Clear();
                Text(Texts.error);
                Text(Texts.edit);
                continue;
            }
            if (deed == -1) { La(); Delete(); }
            else if (deed == 0) { Console.Clear(); return; }
            else
            {
                ALL.RemoveAt(deed - 1);
                Text(Texts.success);
                Text(Texts.cont);
                Console.Clear();
            }
        }
        static void Text(Texts a)
        {
            switch (a)
            {
                case Texts.main:
                    Console.WriteLine("1. Создать новую запить\n2. Редактировать запись\n3. Удалить запись\n4. Просмотреть запись\n5. Посмотреть список записей\n0. Выйти");
                    break;
                case Texts.error:
                    Console.WriteLine("Боюсь, вы ввели неправильное значение\nПопробуйте еще раз");
                    break;
                case Texts.create:
                    Console.WriteLine("Введите поля для записи. Помеченые звездочкой поля являются обязательными");
                    break;
                case Texts.edit:
                    Console.WriteLine("Выберите номер записи для редактирования.\tВведите -1, чтобы посмотреть список записей.\n\t\t\t\t\t\tВведите 0, чтобы вернуться в предыдущее меню.");
                    break;
                case Texts.la:
                    Console.WriteLine("Фамилия\tИмя\tНомер телефона");
                    break;
                case Texts.cat:
                    Console.WriteLine("Введите номер записи для чтения.\tВведите -1, чтобы посмотреть список записей.\n\t\t\t\t\tВведите 0, чтобы вернуться в предыдущее меню.");
                    break;
                case Texts.delete:
                    Console.WriteLine("Введите номер записи для удаления.\tВведите -1, чтобы посмотреть список записей.\n\t\t\t\t\tВведите 0, чтобы вернуться в предыдущее меню.");
                    break;
                case Texts.cont:
                    Console.WriteLine("Нажмите любую клавишу, чтобы продолжить...");
                    Console.ReadKey();
                    break;
                case Texts.success:
                    Console.WriteLine("Успех");
                    break;
                case Texts.props:
                    Console.WriteLine("Выберите поле для изменения:\t\tПомните, что поля, помеченные * не могут быть пустыми");
                    Console.WriteLine("1. Фамилия*\t2. Имя*\t\t3. Отчество\n4. Телефон*\t5. Страна*\t6. Дата рождения\n7. Организация\t8. Должность\t9. Заметки");
                    break;
                default:
                    Console.WriteLine("Андрей, ты ошибился");
                    break;
            }
        }
      

        enum Texts
        {
            main,
            create,
            edit,
            delete,
            cat,
            la,
            error,
            success,
            cont,
            props
        }
        enum MenuChoose
        {
            exit,
            create,
            edit,
            delete,
            cat,
            la
        }
        
    }
}